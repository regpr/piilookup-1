"""
PII Lookup Tool
Version : 0.1
Author: Reginald Ledain
Last Update: 2018-10-05
"""
version = "0.1"

from tkinter import *
import tkinter.messagebox as msg
import tkinter.scrolledtext as tkst
import tkinter.filedialog # , tkinter.messagebox
import pandas as pd
import datetime

now = datetime.datetime.now()
if now.month >= 8:
    current_school_year = now.year+1
else:
    current_school_year = now.year



class Window(object):
    def __init__(self,window):
        global current_school_year
        self.window = window
        self.window.wm_title("PII Lookup Tool v"+version)

        top_frame = Frame(window, width=100, height=50, pady=3)
        top_frame.grid(row=0, sticky="ew")

        # middle_top_frame = Frame(window, background="blue", width=50, height=20, pady=3)
        # middle_top_frame.grid(row=1)

        # w = Label(middle_top_frame, text="Red", bg="red", fg="white")
        # w.pack(fill=X)

        btm_frame = Frame(window, width=450, pady=3)
        btm_frame.grid(row=2, sticky="ew")

        l1 = Label(top_frame, text="You need to have setup an authenticated 64-bit ODBC Connection"
        " called HMH_Redshift_64 and be connected to HMH VPN")
        l1.grid(row=0, column=0,columnspan=4, sticky ="ew")

        self.filename = ""
        school_year = Label(top_frame, text="School Year:").grid(row=1, column=0, sticky ="e")
        self.in_school_year = Entry(top_frame)
        self.in_school_year.insert(END,current_school_year)
        self.in_school_year.grid(row=1, column=1, sticky = W)

        ucn = Label(top_frame, text="District UCN: ").grid(row=1, column=2, sticky="e")
        self.in_ucn = Entry(top_frame)
        self.in_ucn.insert(END, '600009597')
        self.in_ucn.grid(row=1, column=3, sticky = W)

        ucn = Label(btm_frame, text="Paste USER_ID's here (1 per line) or Browse for file: ").grid(row=3, column=0, sticky="ew")

        self.textPad1 = tkst.ScrolledText(btm_frame, width=40, height=20)
        self.textPad1.grid(row=4,column=0)

        self.bwsbutton = Button(btm_frame, text="[1] Browse ", command=self.browsecsv)
        self.bwsbutton.grid(row=3, column=1, sticky ="ns")

        self.lkbutton = Button(btm_frame, text="[2] Lookup > ", command=self.lookup_users)
        self.lkbutton.grid(row=4, column=1, sticky="ew")

        self.svbutton = Button(btm_frame, text="[3] Save to File ", command=self.save_file_as)
        self.svbutton.grid(row=5, column=1, sticky="ew")

        ucn = Label(btm_frame, text="Result of Lookup: ").grid(row=3, column=2, sticky="ew")

        self.textPad2 = tkst.ScrolledText(btm_frame, width=50, height=20)
        self.textPad2.grid(row=4, column=2)

    def RepresentsInt(self,s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def notify_validation(self,label):
        msg.showinfo("Information", "{}".format(label))

    def notify_error(self,label):
        msg.showerror("Error", "{}".format(label))

    def check_form(self):
        cancel_submit = False
        sy = self.in_school_year.get()
        if sy == '' or not self.RepresentsInt(sy):
            cancel_submit = True
            self.notify_validation('Please specify a valid school year!')

        if self.in_ucn.get() == '':
            cancel_submit = True
            self.notify_validation('Please specify the district UCN!')

        return cancel_submit

    def browsecsv(self):
        from tkinter.filedialog import askopenfilename

        Tk().withdraw()
        self.filename = askopenfilename()
        if self.filename != '':

            df = pd.read_csv(self.filename)
            # print(df.columns)
            lower_columns = [c.lower() for c in df.columns]
            if 'user_id' in lower_columns:
                df.columns = lower_columns

                rows = df["user_id"].values
                # self.textPad1.insert("1.0", df["user_id"].values)
                for idx,row in enumerate(rows):

                    self.textPad1.insert(str(idx+1)+".0", row+'\n')
            else:
                self.notify_validation('This file does not contain any column named user_id.\n'
                                       'Please try again or paste the user_ids directly in the left box!')

            #self.textPad1.insert("1.0", df)

    def save_file_as(self, whatever=None):
        self.filename = tkinter.filedialog.asksaveasfilename(defaultextension='.csv', initialfile= self.in_ucn.get()+'_lookup_results', filetypes=[('CSV files', '.csv'),('all files', '.*')])
        if self.filename != '':
            f = open(self.filename, 'w')
            f.write(self.textPad2.get('1.0', 'end'))
            f.close()
            tkinter.messagebox.showinfo('FYI', 'File Saved')

    def lookup_users(self):
        if self.check_form():
            return

        import pyodbc
        try:

            cnxn = pyodbc.connect("DSN=HMH_Redshift_64")
            cursor = cnxn.cursor()
            where_school_year = self.in_school_year.get()
            where_ucn = self.in_ucn.get()
            self.textPad2.delete("1.0",END)

            lines = self.textPad1.get("1.0",END).splitlines()


            where_usersid = "SELECT USER_ID, FIRST_NAME, LAST_NAME FROM STAGING.STUDENTS_DEMOGRAPHICS WHERE SCHOOL_YEAR =" + where_school_year + " AND UCN = '"+ where_ucn +"' AND USER_ID IN ("
            for idx,line in enumerate(lines):
                where_usersid =  where_usersid + "'" + lines[idx]+"'"
                if idx != len(lines)-1:
                    where_usersid = where_usersid +","

            where_usersid = where_usersid + ")"
            rows = cursor.execute(where_usersid)

            for idx,row in enumerate(rows):
                self.textPad2.insert(str(idx+1)+".0", ','.join(str(s) for s in row)+'\n')
            self.textPad2.insert("1.0", "user_id,first_name,last_name\n")
        except Exception as e:
            self.notify_error('I could not connect to the database!  '
            'Verify you have your ODBC DSN is created as HMH_Redshift_64 and that you are connected to VPN!'
            '\n\n{}'.format(e))


#code for the GUI (front end)
window = Tk()
Window(window)

window.mainloop()